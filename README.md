Input boolean expressions, such as `a(b+c)d` or `a*b*c'`. Program tokenizes,
parses, and evaluates input string to display a truth table as well as
sum-of-minterms and product-of-maxterms forms.

