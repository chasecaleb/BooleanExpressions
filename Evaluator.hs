module Evaluator (
                 VarTable,
                 eval,
                 evalAll,
                 minterms,
                 maxterms,
                 sumOfMins,
                 prodOfMaxs,
                 varPerm,
                 variables
                 ) where

import Data.Bits
import Data.Maybe
import Data.List
import Lexer
import Parser

-- Lookup table of variables and respective boolean values.
type VarTable = [(Char, Bool)]

-- Value of boolean expression Tree given VarTable values.
eval :: Tree -> VarTable -> Bool
eval (OpNode op lhs rhs) vars =
        let lval = eval lhs vars
            rval = eval rhs vars
        in
            case op of
                And -> lval .&. rval
                Or  -> lval .|. rval
eval (CompNode t) vars = complement $ eval t vars
eval (BoolNode b) _ = b
eval (VarNode x) vars = fromMaybe (error $ "Variable not found: " ++ [x]) (lookup x vars)

-- Evaluate boolean expression for all possible permutations of variables.
evalAll :: Tree -> [Bool]
evalAll t =
    let vars = variables t
        perms = map (varPerm vars) [0..2^length vars - 1]
    in map (eval t) perms

minterms :: Tree -> [Int]
minterms t = elemIndices True (evalAll t)

maxterms :: Tree -> [Int]
maxterms t = elemIndices False (evalAll t)

-- Create sum-of-minterms string for expression.
-- E.G. Tree for "a'b + a" -> "a'b + ab' + ab"
sumOfMins :: Tree -> String
sumOfMins t = let vts   = map (varPerm $ variables t) (minterms t)
             in intercalate " + " (map termStr vts)
             where
                 termStr :: VarTable -> String
                 termStr ((var, True) : xs)  = var : termStr xs
                 termStr ((var, False) : xs) = var : '\'' : termStr xs
                 termStr []                  = ""

-- Create product of maxterms string for expression.
-- E.G. Tree for "a'b + ab'" -> "(a + b)(a' + b')"
prodOfMaxs :: Tree -> String
prodOfMaxs t = let vts   = map (varPerm $ variables t) (maxterms t)
             in concatMap termStr vts
             where
                 termStr :: VarTable -> String
                 termStr x = "(" ++ termStr' x ++ ")"

                 termStr' ((var, b) : xs)
                    | null xs = var : toComp b
                    | otherwise = [var] ++ toComp b ++ " + " ++ termStr' xs
                 termStr' [] = ""

                 toComp True  = "'"
                 toComp False = ""

-- nth permutation of variable values.
-- I.E. varPerm "abc" 3  == [('a', False), ('b', True), ('c', True)]
varPerm :: String -> Int -> VarTable
varPerm vars n = zipFill vars (binary n) False

-- All unique variables (VarNode chars), alphabetized.
variables :: Tree -> String
variables (OpNode _ lhs rhs) = (sort . nub . concatMap variables) [lhs, rhs]
variables (CompNode t) = (sort . nub . variables) t
variables (VarNode v) = [v]
variables _ = []

-- Convert int to binary digits represented as boolean list.
binary :: Int -> [Bool]
binary 0 = []
binary x
    | x `mod` 2 == 1 = binary (x `div` 2) ++ [True]
    | otherwise      = binary (x `div` 2) ++ [False]

-- Like zip, but *prepend* fill value to ys to make lists even length.
zipFill :: [a] -> [b] -> b -> [(a, b)]
zipFill xs ys fill =
    let extra = replicate (length xs - length ys) fill
    in zip xs (extra ++ ys)
