#!/usr/bin/env runghc
import Lexer
import Parser
import Evaluator

-- TODO/FEATURES:
-- Error handling using Maybes.
-- Simplification via K-mapping.

main :: IO ()
main = do
        -- Get expression tree from user
        putStr "F = "
        expr <- fmap (parse . tokenize) getLine
        -- Print out truth table, sum-of-minterms, and product-of-maxterms.
        putStrLn ""
        putStrLn "Truth Table:"
        mapM_ putStrLn (table expr)
        putStrLn ""
        putStrLn $ "Sum of minterms: " ++ (show . minterms) expr
        putStrLn $ "    " ++ sumOfMins expr
        putStrLn $ "Product of maxterms: " ++ (show . maxterms) expr
        putStrLn $ "    " ++ prodOfMaxs expr

-- List of strings representing truth table. Use mapM_ putStrLn on this.
table :: Tree -> [String]
table t =
    let v      = variables t
        nPerms = 2^length v - 1
    in header v : map (row t . (varPerm . variables) t) [0..nPerms]

-- Truth table header: variable names.
header :: String -> String
header xs = header' xs ""
    where
        header' [] s     = s ++ "|| F"
        header' (x':xs') s = header' xs' (s ++ "| " ++ [x'] ++ " ")

-- Truth table row: variable and function values.
row :: Tree -> VarTable -> String
row tree vars = varStr vars "" ++ "|| " ++ show' (eval tree vars)
    where
        varStr [] s = s
        varStr ((_, x):xs) s = varStr xs (s ++ "| " ++ show' x ++ " ")
        show' True = "1"
        show' False = "0"
