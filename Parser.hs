module Parser (
              Tree(..),
              parse
              ) where

import Lexer

data Tree = OpNode Operator Tree Tree
          | CompNode Tree
          | BoolNode Bool
          | VarNode Char
    deriving (Show, Eq)

-- Turn list of tokens into a grammar/expression tree.
parse :: [Token] -> Tree
parse toks = let (tree, toks') = expression toks
             in if null toks' then tree
                              else error $ "Unparsed tokens: " ++ show toks'


-- term + or operator + expression
-- term
expression :: [Token] -> (Tree, [Token])
expression toks = let (lhs, toks') = term toks
                  in case headM toks' of
                         Just (TokOp Or) ->
                            let (rhs, toks'') = expression . tail $ toks'
                            in (OpNode Or lhs rhs, toks'')
                         _ -> (lhs, toks')

-- factor + and operator + term
-- complement
-- factor
term :: [Token] -> (Tree, [Token])
term toks = let (facTree, toks') = factor toks
            in case headM toks' of
                   Just (TokOp And) ->
                        let (termTree, toks'') = term . tail $ toks'
                        in (OpNode And facTree termTree, toks'')
                   Just TokComp ->
                        case headM $ tail toks' of
                            Just (TokOp And) ->
                                let (termTree, toks'') = term . tail . tail $ toks'
                                in (OpNode And (CompNode facTree) termTree, toks'')
                            Just (TokVar _) ->  -- AND is implied
                                let (termTree, toks'') = term . tail $ toks'
                                in (OpNode And (CompNode facTree) termTree, toks'')
                            Just TokLParen ->  -- AND is implied
                                let (termTree, toks'') = term . tail $ toks'
                                in (OpNode And (CompNode facTree) termTree, toks'')
                            _ -> (CompNode facTree, tail toks')
                   _ -> (facTree, toks')

-- Bool
-- Variable
-- Variable + (implied and) + variable
-- Variable + (implied and) + LParen
-- Parentheses
factor :: [Token] -> (Tree, [Token])
factor toks = case headM toks of
                Just (TokBool b)   -> (BoolNode b, tail toks)
                Just (TokVar v)    ->
                    case headM  . tail $ toks of
                        Just (TokVar _) ->
                            factor (head toks : TokOp And : tail toks)
                        Just TokLParen ->
                            factor (head toks : TokOp And : tail toks)
                        _ -> (VarNode v, tail toks)
                Just TokLParen ->
                    let (expTree, toks') = expression . tail $ toks
                    in
                        if headM toks' /= Just TokRParen
                        then error "Missing right parentheses"
                        else (expTree, tail toks')
                _ -> error $ "Factor error at: " ++ show toks

-- Maybe version of head.
headM :: [a] -> Maybe a
headM [] = Nothing
headM (c:_) = Just c
