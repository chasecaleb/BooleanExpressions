module Lexer (
             Operator(..),
             Complement(..),
             Token(..),
             tokenize
             ) where

import Data.Char

data Operator = And | Or
    deriving (Show, Eq)
data Complement = Pre | Post
    deriving (Show, Eq)

data Token = TokOp Operator
           | TokBool Bool
           | TokVar Char
           | TokLParen
           | TokRParen
           | TokComp
    deriving (Show, Eq)

-- Convert boolean algebra string to array of tokens.
tokenize :: String -> [Token]
tokenize = map tokenizeChar . filter (not . isSpace)
    where
        tokenizeChar c
            | c `elem` "*+"  = (TokOp . operator) c
            | c `elem` "1tT" = TokBool True
            | c `elem` "0fF" = TokBool False
            | isAlpha c      = TokVar c
            | c == '('       = TokLParen
            | c == ')'       = TokRParen
            | c == '\''      = TokComp
            | otherwise      = error $ "Cannot tokenize " ++ [c]

-- Convert char to boolean algebra operator (e.g. '*' -> and).
operator :: Char -> Operator
operator c
    | c == '*'  = And
    | c == '+'  = Or
    | otherwise = error $ c : " is not an operator"
